# If tge first installation (not update)
if [ ! -f ~/.shehata_terminal/replaced_files/installed.flag ]; then
  if [[ -d ~/.vim  && ! -L ~/.vim ]]
    rm -rf ~/.shehata_terminal/replaced_files/.vim
    then mv -f ~/.vim ~/.shehata_terminal/replaced_files/
  fi

  if [ -f ~/.vimrc ]; then
    mv -f ~/.vimrc ~/.shehata_terminal/replaced_files/.vimrc
  fi

  ln -s ~/.shehata_terminal/vim/.vimrc ~/.vimrc

  # Insall Vundle.vim if not installed
  mkdir -p ~/.vim/bundle/;
  if [[ ! -d ~/.vim/bundle/Vundle.vim ]]; then
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim;
  fi

  if ! grep 'source ~/.shehata_terminal/bash/bash_profile.sh' ~/.bash_profile; then
    printf "\nsource ~/.shehata_terminal/bash/bash_profile.sh\n" >> ~/.bash_profile
  fi
fi

vim +PluginInstall +qall;

if [ ! -d ~/.fzf ]; then
  git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
  ~/.fzf/install --key-bindings --completion --no-update-rc
fi

touch ~/.shehata_terminal/replaced_files/installed.flag

# TODO: Fix this it will move the file each time!
if [ -f ~/.tmux.conf ]; then
  mv -f ~/.tmux.conf ~/.shehata_terminal/replaced_files/.tmux.conf
fi

ln -s ~/.shehata_terminal/tmux/.tmux.conf ~/.tmux.conf

ln ~/.shehata_terminal/mycli/.myclirc ~/

