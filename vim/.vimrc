
" +       : in visual mode to increase the selection
" _       : in visual mode to decrease the selection
" :Gblame : git blame


"============================================== Vundle.vim =========================================
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'joshdick/onedark.vim'
Plugin 'mileszs/ack.vim' " Run your favorite search tool
Plugin 'sjl/vitality.vim' " Makes Vim play nicely with iTerm 2 and tmux.
Plugin 'itchyny/lightline.vim' " Status line
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'tpope/vim-commentary' " Comment stuff out
Plugin 'terryma/vim-expand-region' "  visually select increasingly larger regions
Plugin 'tpope/vim-fugitive' " Git
Plugin 'vim-scripts/AutoComplPop'
Plugin 'ervandew/supertab' " use <Tab> for all your insert completion
Plugin 'MattesGroeger/vim-bookmarks'
Plugin 'junegunn/fzf' " general-purpose command-line fuzzy finde
Plugin 'junegunn/fzf.vim'
Plugin 'tpope/vim-sleuth' " automatically adjusts 'shiftwidth' and 'expandtab' heuristically based on the current file
Plugin 'michaeljsmith/vim-indent-object'
Plugin 'wellle/targets.vim' " text objects
Plugin 'tpope/vim-surround'
Plugin 'romainl/vim-cool' " disables search highlighting when you are done searching and re-enables it when you search again
Plugin 'kana/vim-textobj-user' " Create your own text objects!
Plugin 'kana/vim-textobj-entire' " textobj for the entire file
Plugin 'nelstrom/vim-textobj-rubyblock'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-abolish' " change from camel case to snake case, easily search for, substitute, and abbreviate multiple variants of a word.
Plugin 'Raimondi/delimitMate'

call vundle#end()            " required
filetype on
filetype plugin on

"=======================================================================================
let mapleader=" "
let g:SuperTabDefaultCompletionType = "<c-n>" " default to scroll down the list

set laststatus=2
set noshowmode
set mouse=a         " enables mouse in all modes
set tabstop=2       " number of visual spaces per TAB
set softtabstop=2   " number of spaces in tab when editing
set shiftwidth=2
set expandtab
set smarttab
set ai "Auto indent
set si "Smart indent
set nowrap "No wrap lines
set incsearch
set number
set relativenumber
set hlsearch            " highlight matches
set showmatch
set wildmenu            " visual autocomplete for command menu
set omnifunc=syntaxcomplete#Complete   " filetype-specific completion
set autoread " Set to auto read when a file is changed from the outside
set backspace=indent,eol,start " backspace work like in most other programs : https://vi.stackexchange.com/questions/2162/why-doesnt-the-backspace-key-work-in-insert-mode
set spell
set cmdheight=1
set ignorecase " Ignore case when searching
set smartcase " When searching try to be smart about cases
set lazyredraw " Don't redraw while executing macros (good performance config)
set magic " For regular expressions turn magic on
set showmatch " Show matching brackets when text indicator is over them
set mat=2 " How many tenths of a second to blink when matching brackets
set noerrorbells  " No annoying sound on errors
set novisualbell
set t_vb=
set tm=500
set fillchars+=vert:\│ " The vertical separator
set wildmode=longest:full,full " https://stackoverflow.com/questions/11503040/how-to-change-auto-complete-behaviour-in-vim
set clipboard=unnamed
set history=2000

execute "set colorcolumn=" . join(range(101,335), ',')
"============================ Mappings ==========================
nmap oo 0i<CR><Esc>k " Insert line
noremap <C-h> :History<CR>
"noremap <C-g> :GGrep<CR>
noremap <C-t> :tabnew<CR>

" Go to tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>

noremap <leader>w :w!<cr>
noremap <leader>q :q<cr>
noremap <leader>p A <Esc>p
noremap <leader>t :tabnew<CR>

map <leader>ss :setlocal spell!<cr>
map <leader>pp :setlocal paste!<cr>

nnoremap <S-Tab> <<
nnoremap <Tab> >>

noremap <C-p> :call fzf#vim#gitfiles('', fzf#vim#with_preview('right'))<CR>

noremap <C-j> :tabprevious<CR>
noremap <C-k> :tabNext<CR>
tnoremap <Esc> <C-\><C-n>

vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR> " Visual mode pressing * or # searches for the current selection
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR> " Visual mode pressing * or # searches for the current selection

map <silent> <leader><cr> :noh<cr> " disable highlight when <leader><cr> is pressed
"========================= Line Objects =========================
xnoremap il g_o0
onoremap il :normal vil<CR>
xnoremap al $o0
onoremap al :normal val<CR>

"============================ Colors ===========================
colorscheme onedark
syntax enable
hi CursorLine term=bold cterm=bold ctermbg=236 "High light the cursor line
set cursorline
hi NonText ctermfg=235 ctermbg=235 "Removing tilde characters in front of non-existing lines
highlight ColorColumn ctermbg=235 guibg=lightgrey

"=========================== Goyo =============================
let g:goyo_height = "100%"
function! s:goyo_leave()
    call SetColors()
endfunction
autocmd! User GoyoLeave nested call <SID>goyo_leave()

"=========================== Ack =============================
" Use the silver_searcher if possible (much faster than Ack)
if executable('ag')
  let g:ackprg = 'ag --vimgrep --smart-case'
endif
"======================= Multi Cursor  ========================
function! Multiple_cursors_before()
  let g:SuperTabDefaultCompletionType = ""
endfunction

" Enable autocomplete after multiple cursors
function! Multiple_cursors_after()
  let g:SuperTabDefaultCompletionType = "<c-n>"
endfunction
"==============================================================

"https://stackoverflow.com/questions/1675688/make-vim-show-all-white-spaces-as-a-character
set listchars=tab:>-,trail:~,extends:>,precedes:<
set list


" Let 'tl' toggle between this and the last accessed tab
let g:lasttab = 1
nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

" Specify the behavior when switching between buffers
try
  set switchbuf=useopen,usetab,newtab
  set stal=2
catch
endtry

" Return to last edit position when opening files (You want this!)
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif


""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c

nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
  nmap <D-j> <M-j>
  nmap <D-k> <M-k>
  vmap <D-j> <M-j>
  vmap <D-k> <M-k>
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pressing ,ss will toggle and untoggle spell checking
"=============================================================

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=


function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    endif
    return ''
endfunction

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
   let l:currentBufNum = bufnr("%")
   let l:alternateBufNum = bufnr("#")

   if buflisted(l:alternateBufNum)
     buffer #
   else
     bnext
   endif

   if bufnr("%") == l:currentBufNum
     new
   endif

   if buflisted(l:currentBufNum)
     execute("bdelete! ".l:currentBufNum)
   endif
endfunction

function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction

function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", "\\/.*'$^~[]")
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ack '" . l:pattern . "' " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Turn persistent undo on
"    means that you can undo even when you close a buffer/VIM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try
    set undodir=~/.vim_runtime/temp_dirs/undodir
    set undofile
catch
endtry

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Parenthesis/bracket
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
vnoremap $1 <esc>`>a)<esc>`<i(<esc>
vnoremap $2 <esc>`>a]<esc>`<i[<esc>
vnoremap $3 <esc>`>a}<esc>`<i{<esc>
vnoremap $$ <esc>`>a"<esc>`<i"<esc>
vnoremap $q <esc>`>a'<esc>`<i'<esc>
vnoremap $e <esc>`>a"<esc>`<i"<esc>

" Map auto complete of (, ", ', [
inoremap $1 ()<esc>i
inoremap $2 []<esc>i
inoremap $3 {}<esc>i
inoremap $4 {<esc>o}<esc>O
inoremap $q ''<esc>i
inoremap $e ""<esc>i

" When you press gv you Ack after the selected text
vnoremap <silent> gv :call VisualSelection('gv', '')<CR>

" Open Ack and put the cursor in the right position
map <leader>g :Ack

" When you press <leader>r you can search and replace the selected text
vnoremap <silent> <leader>r :call VisualSelection('replace', '')<CR>

" Do :help cope if you are unsure what cope is. It's super useful!
"
" When you search with Ack, display your results in cope by doing:
"   <leader>cc
"
" To go to the next search result do:
"   <leader>n
"
" To go to the previous search results do:
"   <leader>p
"
map <leader>cc :botright cope<cr>
map <leader>co ggVGy:tabnew<cr>:set syntax=qf<cr>pgg
map <leader>n :cn<cr>
" map <leader>p :cp<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Nerd Tree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let NERDTreeShowHidden=0
let NERDTreeIgnore = ['\.pyc$', '__pycache__']
let g:NERDTreeWinSize=35
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let g:nerdtree_tabs_open_on_console_startup=2
let g:nerdtree_tabs_smart_startup_focus=2
map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark<Space>
map <leader>nf :NERDTreeFind<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" filetype
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" for .hql files
au BufNewFile,BufRead *.hql set filetype=hive expandtab

" for .q files
au BufNewFile,BufRead *.q set filetype=hive expandtab


""""""""""""""""
" :autocmd CursorMoved * call SendYanked()
" :autocmd CursorHoldI * call SendYanked()
" :autocmd CursorMoved * call SendYanked()
" :autocmd CursorMovedI * call SendYanked()
" :autocmd FocusGained * call SendYanked()
" :autocmd FocusLost * call SendYanked()
" :autocmd CursorMovedI * call SendYanked()
" :autocmd TabLeave * call SendYanked()
" :autocmd CmdwinEnter * call SendYanked()

" function! SendYanked()
"   if @r == @0
"     return
"   endif

"   let @r = @0
"   :call writefile(split(@0, "\n", 1), '/tmp/yank.out')
" endfunction

" map <leader>y :call SendYanked()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" https://github.com/noahfrederick/dots/blob/83f0fbbed475769324d3c39e87e54aae1bba60fc/vim/plugin/blink_search.vim
" blink_search.vim - Blink search pattern after n and N
" Maintainer:   Noah Frederick

if exists("g:loaded_blink_search") || v:version < 700 || &cp
  finish
endif
let g:loaded_blink_search = 1

nnoremap <silent> n n:call <SID>BlinkCurrentMatch()<CR>
nnoremap <silent> N N:call <SID>BlinkCurrentMatch()<CR>

function! s:BlinkCurrentMatch()
  let target = '\c\%#'.@/
  let match = matchadd('IncSearch', target)
  redraw
  sleep 100m
  call matchdelete(match)
  redraw
endfunction

" vim: fdm=marker:sw=2:sts=2:et
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""Lightline"""""""""""""""""""""""
let g:lightline = {
	\ 'colorscheme': 'onedark',
	\ 'active': {
	\   'left': [ [ 'mode', 'paste' ],
	\             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
	\ },
	\ 'component_function': {
	\   'gitbranch': 'fugitive#head',
	\   'filename': 'LightlineFilename'
	\ },
\ }

function! LightlineFilename()
  return expand('%')
endfunction

"======================= Session ========================
" Quick write session with F2
map <F2> :mksession! ~/.vim_session<CR>
" And load session with F3
map <F3> :call Load_session()<CR>

function! Load_session()
  source ~/.vim_session
  call SetColors()
endfunction
"========================= fzf ===========================
set rtp+=~/.fzf

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

let $FZF_DEFAULT_OPTS = '--bind ctrl-a:select-all'

" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1

" [[B]Commits] Customize the options used by 'git log':
let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'

" [Tags] Command to generate tags file
let g:fzf_tags_command = 'ctags -R'

" [Commands] --expect expression for directly executing the command
let g:fzf_commands_expect = 'alt-enter,ctrl-x'

" Command for git grep
" - fzf#vim#grep(command, with_column, [options], [fullscreen])
command! -bang -nargs=* GGrep
  \ call fzf#vim#grep(
  \   'git grep --line-number '.shellescape(<q-args>), 0,
  \   { 'dir': '.'}, <bang>0)

" Override Colors command. You can safely do this in your .vimrc as fzf.vim
" will not override existing commands.
"command! -bang Colors
"  \ call fzf#vim#colors({'left': '15%', 'options': '--reverse --margin 30%,0'}, <bang>0)

" Augmenting Ag command using fzf#vim#with_preview function
"   * fzf#vim#with_preview([[options], preview window, [toggle keys...]])
"     * For syntax-highlighting, Ruby and any of the following tools are required:
"       - Highlight: http://www.andre-simon.de/doku/highlight/en/highlight.php
"       - CodeRay: http://coderay.rubychan.de/
"       - Rouge: https://github.com/j/een/rouge
"
"   :Ag  - Start fzf with hidden preview window that can be enabled with "?" key
"   :Ag! - Start fzf in fullscreen and display the preview window above
command! -bang -nargs=* Ag
  \ call fzf#vim#ag(<q-args>,
  \                 <bang>0 ? fzf#vim#with_preview('up:60%')
  \                         : fzf#vim#with_preview('right:50%:hidden', '?'),
  \                 <bang>0)

" Similarly, we can apply it to fzf#vim#grep. To use ripgrep instead of ag:
command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)

" Likewise, Files command with preview window
command! -bang -nargs=? -complete=dir Files
  \ call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)

"======================== Whitespace ====================
fun! TrimWhitespace()
    let l:save = winsaveview()
    %s/\s\+$//e
    call winrestview(l:save)
endfun

command! TrimWhitespace call TrimWhitespace()
autocmd BufWritePre * :call TrimWhitespace()
"===================== File types =============================
au BufNewFile,BufRead *.hql set filetype=sql
