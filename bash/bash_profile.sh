RED="\[\033[0;31m\]"
YELLOW="\[\033[0;33m\]"
GREEN="\[\033[0;32m\]"
NO_COLOR="\[\033[0m\]"

if [ -f ~/.shehata_terminal/bash/local_settings.sh ]; then
  source ~/.shehata_terminal/bash/local_settings.sh
fi

if [[ `screen -ls` != *"5556_to_bpcpy"* && "$IS_THE_PRESONAL_OSX_COMPUTER" = "true" ]]; then
  # the head cmd for remving the extra new line character from nc
  screen -dmS 5556_to_bpcpy bash -c '
    while (true); do
      s="$(nc -l 5556)";
      echo "${s}" | head -c `expr ${#s}` | pbcopy;
    done
  '
fi

if [[ `screen -ls` != *"yank_watcher"* ]]; then
  screen -dmS yank_watcher bash -c 'sh ~/.shehata_terminal/bash/yank_watcher.sh'
fi

# fzf
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export VISUAL=vim
export EDITOR="$VISUAL"

export CLICOLOR=1
export LSCOLORS="gxBxhxDxfxhxhxhxhxcxcx"

alias ll='ls -lh'
alias grep='grep --color=auto'
alias tailf='tail -f'
alias repl='re.pl'
alias python='python3'
alias vi='vim'
alias s='sublime'

######### GIT ########
alias gc='git commit -m '
alias gl='git log --all --decorate --oneline --graph'
alias gs='git status'
alias ga='git add --all'
alias gd='git diff'
alias push="git push"
alias pull="git pull"

function mount_ticketing() {
  echo $1;
  umount -f ~/remote/ticketing;
  sshfs -p 22 mshehata@mshehata-$1.dev.booking.com:/usr/local/git_tree/main/apps/ticketing ~/remote/ticketing -o auto_cache,reconnect,defer_permissions,noappledouble,negative_vncache,volname=ticketing;
}

function mount_main() {
  echo $1;
  umount -f ~/remote/main;
  sshfs -p 22 mshehata@mshehata-$1.dev.booking.com:/usr/local/git_tree/main ~/remote/main -o auto_cache,reconnect,defer_permissions,noappledouble,negative_vncache,volname=maingit;
}

function parse_git_branch () {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Set defaults
MACHINE_NAME=${MACHINE_NAME:-machine}
MACHINE_NAME_COLOR=${MACHINE_NAME_COLOR:-$GREEN}

# Two lines:
# PS1=" [\$(date +\"%I:%M %p\")] $MACHINE_NAME_COLOR$MACHINE_NAME$NO_COLOR:\W$YELLOW\$(parse_git_branch)$NO_COLOR\[\e[0;31m\]\n▸▸ \[\e[0m\]"
PS1="$MACHINE_NAME_COLOR$MACHINE_NAME$NO_COLOR:\W$YELLOW\$(parse_git_branch)$NO_COLOR\[\e[0;31m\]▸ \[\e[0m\]"

############################### SSH agent fix #################################
find_ssh_agent(){
  agent_socket_file=$(find /tmp -maxdepth 2 -type s -name "agent*" -user $USER -printf '%T@ %p\n' 2>/dev/null |sort -n|tail -1|cut -d' ' -f2)
  if [ -n "$agent_socket_file" ]; then
    export SSH_AUTH_SOCK=$HOME/.ssh/ssh_auth_sock
    ln -sf $agent_socket_file ~/.ssh/ssh_auth_sock
  fi
}
###############################################################################
